# Changelog

## 5.1.5

- Basic detection of draw by insufficient material

## 5.1.6

- New translation system (PO files)

