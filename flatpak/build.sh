function askYesNo {
        QUESTION=$1
        DEFAULT=$2
        if [ "$DEFAULT" = true ]; then
                OPTIONS="[Y/n]"
                DEFAULT="y"
            else
                OPTIONS="[y/N]"
                DEFAULT="n"
        fi
        read -p "$QUESTION $OPTIONS " -n 1 -s -r INPUT
        INPUT=${INPUT:-${DEFAULT}}
        echo ${INPUT}
        if [[ "$INPUT" =~ ^[yY]$ ]]; then
            ANSWER=true
        else
            ANSWER=false
        fi
}



cd $( dirname -- "$( readlink -f -- "$0"; )"; )
rm -rf eschecs/ eschecs.zip build-dir .flatpak-builder
mkdir eschecs
cd ..
askYesNo "Do you want to build the app with make ?" true
DOIT=$ANSWER

if [ "$DOIT" = true ]; then
    cd source
    make
    cd assistant
    make
    cd ../../
fi

cp assistant flatpak/eschecs
cp -r audio flatpak/eschecs
cp -r books flatpak/eschecs
cp -r config flatpak/eschecs
cp -r engines flatpak/eschecs
cp eschecs flatpak/eschecs
cp -r images flatpak/eschecs
cd flatpak/
zip -r eschecs.zip assistant audio books config engines eschecs images
flatpak-builder build-dir com.gitlab.rchastain.Eschecs.yml
askYesNo "Do you want to install the flatpak ?" true
DOIT=$ANSWER

if [ "$DOIT" = true ]; then
    rm -rf build-dir .flatpak-builder
    flatpak-builder --user --install --force-clean build-dir com.gitlab.rchastain.Eschecs.yml
fi

echo Enjoy \;\)
