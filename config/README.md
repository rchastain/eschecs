# Install a new engine

To install a new engine, you have to edit *linux.ini* or *windows.ini*.

When you have edited the INI file, you can use the script *afteredit.lua* to redo the numerotation of the sections:

```
lua afteredit.lua linux.ini
```
