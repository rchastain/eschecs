
function FixSectionsName(AText)
  local LPattern = '%[engine%d+%]'
  local LIndex = 0
  local i, j = AText:find(LPattern)
  while i ~= nil do
    local LMatch = AText:sub(i, j)
    io.write('DEBUG --->' .. LMatch .. '<---\n')
    AText = AText:sub(1, i - 1) .. string.format('[engine%d]', LIndex) .. AText:sub(j + 1)
    i, j = AText:find(LPattern, i + 1)
    LIndex = LIndex + 1
  end
  return AText
end

if #arg == 1 then
  local LFile = io.open(arg[1], 'r')
  local LText = LFile:read('*a')
  LFile:close()
  
  LText = FixSectionsName(LText)
  
  LFile = io.open(arg[1], 'w')
  LFile:write(LText)
  LFile:close()
else
  io.write('Usage: lua afteredit.lua [filename].ini')
end
