
unit Engines;

interface

uses
  SysUtils,
  IniFiles;
  
type
  TEngineInfo = record
    FCommand, FName, FDirectory: string;
    FExists: boolean;
  end;

var
  LEngines: array of TEngineInfo;

procedure LoadEnginesData(const AFileName: TFileName);

implementation

procedure LoadEnginesData(const AFileName: TFileName);
var
  LSection, LDirectory: string;
  x: integer = 0;
begin
  with TIniFile.Create(AFileName) do
  try
    while x >= 0 do 
    begin
      LSection := 'engine' + IntToStr(x);
      if Length(ReadString(LSection, 'name', EmptyStr)) > 0 then
      begin
        SetLength(LEngines, Succ(x));
        with LEngines[x] do
        begin
          FName := ReadString(LSection, 'name', EmptyStr);
          FCommand := ReadString(LSection, 'command', EmptyStr);
          FDirectory := ReadString(LSection, 'directory', EmptyStr);
          LDirectory := Concat(ExtractFilePath(ParamStr(0)), FDirectory);
          if DirectoryExists(LDirectory) then
            FDirectory := LDirectory;
          FExists := FileExists(Concat(FDirectory, FCommand));
        end;
        Inc(x);
      end else
        x := -1;
    end;
  finally
    Free;
  end;
end;

finalization
  SetLength(LEngines, 0);
  
end.
