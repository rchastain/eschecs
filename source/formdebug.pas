
unit FormDebug;

interface

uses
  Classes,
  SysUtils,
  fpg_main,
  fpg_form,
  fpg_button,
  fpg_memo;
  
type
  TFormDebug = class(TfpgForm)
  private
    Memo1: TfpgMemo;
    btnClose: TfpgButton;
    procedure   btnCloseClick(Sender: TObject);
  public
    constructor Create(AOwner: TComponent); override;
  end;

implementation

{$I icon}

{ TFormDebug }

procedure TFormDebug.btnCloseClick(Sender: TObject);
begin
  Close;
end;

constructor TFormDebug.Create(AOwner: TComponent);
var
  LLogFile: string;
begin
  inherited Create(AOwner);
  Visible := False;
  WindowTitle := 'Log';
  Sizeable := False;
  IconName := 'vfd.eschecs';
  WindowPosition := wpOneThirdDown;
  SetPosition(0, 0, 600, 400);
  
  LLogFile := ExtractFilePath(ParamStr(0)) + 'eschecs.log';
  
  Memo1 := TfpgMemo.Create(self);
  with Memo1 do
  begin
    Name := 'Memo1';
    SetPosition(4, 4, 592, 360);
    Anchors := [anLeft, anRight, anTop, anBottom];
    Hint := '';
    if FileExists(LLogFile) then
      Lines.LoadFromFile(LLogFile);
    FontDesc := '#Edit2';
    TabOrder := 0;
  end;
  
  btnClose := CreateButton(self, 516, 368, 80, 'Close', @btnCloseClick);
  btnClose.Anchors := [anRight, anBottom];
  btnClose.ImageName := 'stdimg.Quit';
  btnClose.ShowImage := True;
end;

end.

