
uses
  Uci;

const
  CMsg1 =
  'id name Pharaon 3.5.1' + LineEnding +
  'id author Franck ZIBI' + LineEnding +
  'option name Nullmove type check default TRUE' + LineEnding +
  'option name Ponder type check default TRUE' + LineEnding +
  'option name Clear Hash type button' + LineEnding +
  'option name Hash type spin min 1 max 1024 default 64' + LineEnding +
  'option name NalimovPath type string default ;./TB;C:\CHESS\TB;d:\Pharaon\tb;' + LineEnding +
  'option name NalimovCache type spin min 1 max 32 default 4' + LineEnding +
  'option name Number of threads type spin min 1 max 4 default 1' + LineEnding +
  'option name UCI_Chess960 type check default FALSE' + LineEnding +
  'copyprotection checking' + LineEnding +
  'copyprotection ok' + LineEnding +
  'uciok';
  CMsg2 = 'bestmove a7a8q';
  CMsg3 = 'bestmove a7a8 ';

var
  LName, LAuthor, LMove, LPromo: string;
  LFrcSupport: boolean;
  
begin
  WriteLn(MsgUci = 'uci');
  
  WriteLn(IsMsgId(CMsg1, LName, LAuthor));
  WriteLn(LName = 'Pharaon 3.5.1');
  
  WriteLn(IsMsgOptions(CMsg1, LFrcSupport));
  WriteLn('LFrcSupport = ', LFrcSupport);
  
  WriteLn(IsMsgUciOk(CMsg1));
  
  WriteLn(IsMsgBestMove(CMsg2, LMove, LPromo));
  WriteLn(LPromo = 'q');
  
  WriteLn(IsMsgBestMove(CMsg3, LMove, LPromo));
  WriteLn(LPromo = '');
end.
