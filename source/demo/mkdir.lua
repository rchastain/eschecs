
-- mkdir.lua
-- Create a directory
-- Multiplatform (Linux, Windows)
-- Usage: lua mkdir.lua DIRECTORY_NAME

function IsWindows()
-- https://stackoverflow.com/questions/295052/how-can-i-determine-the-os-of-the-system-from-within-a-lua-script
-- You can try package.config:sub(1,1). It returns the path separator, which is '\\' on Windows and '/' on Unixes...
  return package.config:sub(1, 1) == '\\'
end

if #arg == 1 then
  local lNotExecuted = 'Command not executed'
  local lHelp = 'Usage: lua mkdir.lua DIRECTORY_NAME'
  local lCmd
  
  if IsWindows() then
    lCmd = string.format('if not exist %s mkdir %s', arg[1], arg[1])
  else
    lCmd = string.format('mkdir -p %s', arg[1])
  end
  
  if not os.execute(lCmd) then
    io.write(lNotExecuted, '\n')
  end
else
  io.write(lHelp, '\n')
end
