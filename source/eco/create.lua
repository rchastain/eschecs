
function ReadText(AFileName)
  local file = io.open(AFileName, 'r')
  local text = file:read('*a')
  file:close()
  return text
end

function WriteText(AFileName, AText)
  file = io.open(AFileName, 'w')
  file:write(AText)
  file:close()
end

if #arg == 4 then
  local text = ReadText(arg[1])
  
  local n = 0
  text, n = string.gsub(text, "_REPLACE1_", arg[3])
  text, n = string.gsub(text, "_REPLACE2_", ReadText(arg[4]))
  
  WriteText(arg[2], text)
else
  print('Paramètres manquants')
end
