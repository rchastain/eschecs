﻿
{$I DIRECTIVES}

uses
  SysUtils, Classes, IOUtils, FLRE;

const
(*
  CPattern = '([A-E][0-9]{2}) ([A-Z][ \w():''\-,/\.]+)\r\n *1\.([\sa-h1-8]+[1-8])';
*)
  CPattern =
    '([A-E][0-9]{2})' +
    '\t' +
    '(.+?)' +
    '\t' +
    '(.+?)' +
    '\t' +
    '([a-h1-8\s]+)' +
    '\t' +
    '(.+?)' +
    '\n';

var
  LExpr: TFLRE;
  ss: TFLREMultiStrings;
  i: integer;
  LAux1, LAux2: ansistring;
  LPasFile, LJsonFile: textfile;
begin  
  AssignFile(LPasFile, 'eco.pas');
  Rewrite(LPasFile);
  AssignFile(LJsonFile, 'eco.json');
  Rewrite(LJsonFile);
  
  LExpr := TFLRE.Create(CPattern, []);
  if LExpr.ExtractAll(RawByteString(TFile.ReadAllText('all.tsv')), ss) then
  begin
    WriteLn(Length(ss), 'results');
    
    for i := 0 to Length(ss) - 1 do
    begin
    (*
      LAux1 := StringReplace(ss[i, 3], #32, '', [rfReplaceAll]);
      LAux1 := StringReplace(LAux1, #13, '', [rfReplaceAll]);
      LAux1 := StringReplace(LAux1, #10, '', [rfReplaceAll]);
    *)
      LAux1 := StringReplace(ss[i, 4], #32, '', [rfReplaceAll]);
      LAux2 := StringReplace(ss[i, 2], '''', '''''', [rfReplaceAll]);
      
      WriteLn(LPasFile, Format('    (FCode: ''%s''; FName: ''%s''; FMoves: ''%s''; FPosition: ''%s''),', [ss[i, 1], LAux2, LAux1, ss[i, 5]]));
      
      WriteLn(LJsonFile, Format('"%s" : "%s %s",', [LAux1, ss[i, 1], ss[i, 2]]));
    end;
  end;
  SetLength(ss, 0);
  LExpr.Free;
  
  CloseFile(LPasFile);
  CloseFile(LJsonFile);
end.
