{**
@abstract(Encyclopédie des ouvertures.)
}

unit Eco;

interface

function GetOpening(const AMoves: string): string;

implementation

uses
  Classes;

type
  TOpening = record
    FCode, FName, FMoves, FPosition: string;
  end;

const
  CData: array[1.._REPLACE1_] of TOpening = (
_REPLACE2_  );

type
  TIntClass = class
    FInt: integer;
  end;
  
var
  LList: TStringList;
 
function GetOpening(const AMoves: string): string;
var
  i: integer;
begin
  if LList.Find(AMoves, i) then
  begin
    i := TIntClass(LList.Objects[i]).FInt;
    result := Concat(CData[i].FCode, ' ', CData[i].FName);
  end else
    result := '';
end;

var
  i: integer;
  c: TIntClass;

initialization
  LList := TStringList.Create;
  LList.Sorted := TRUE;
  for i := Low(CData) to High(CData) do
  begin
    c := TIntClass.Create;
    c.FInt := i;
    LList.AddObject(CData[i].FMoves, TObject(c));
  end;
  
finalization
  for i := 0 to Pred(LList.Count) do
    LList.Objects[i].Free;
  LList.Free;
  
end.
