
unit Sound;

interface

const
  sndCapture = 0;
  sndCheck = 1;
  sndEndOfGame = 2;
  sndIllegal = 3;
  sndMove = 4;
  sndPromotion = 5;
    
function LoadSoundLib(const AUseSystemLib: boolean = FALSE): integer;
procedure Play(const ASound: integer);
procedure SetSoundVolume(AVol: shortint);
procedure FreeUos;

implementation

uses
  SysUtils, Classes,
  uos_flat,
  Utils;
  
const
  CFileName: array[0..5] of string = (
    'capture.mp3',
    'check2.mp3',
    'end.mp3',
    'illegal2.mp3',
    'move.mp3',
    'promotion.mp3'
  );  
  
var
  LStreams:  array[0..5] of TMemoryStream; 
  LLoaded: boolean = FALSE;

procedure Play(const ASound: integer);
begin
  if LLoaded then
    uos_PlayNoFree(ASound);
end;

function LoadSoundLib(const AUseSystemLib: boolean): integer;
var
  LAudioPath, LSndFilesPath, LPortAudio, LMpg123, LSndFile: string;
  x: integer;
begin
  LAudioPath := ExtractFilePath(ParamStr(0)) + 'audio' + DirectorySeparator;
  LSndFilesPath := LAudioPath + 'sound' + DirectorySeparator;
{$IFDEF windows}
{$IF DEFINED(cpu64)}
  LPortAudio := LAudioPath + 'lib\Windows\64bit\LibPortaudio-64.dll';
  LMpg123 := LAudioPath + 'lib\Windows\64bit\LibMpg123-64.dll';
{$ELSE}
  LPortAudio := LAudioPath + 'lib\Windows\32bit\LibPortaudio-32.dll';
  LMpg123 := LAudioPath + 'lib\Windows\32bit\LibMpg123-32.dll';
{$ENDIF}
{$ENDIF}
{$IF DEFINED(cpu64) and defined(linux) }
  LPortAudio := LAudioPath + 'lib/Linux/64bit/LibPortaudio-64.so';
  LMpg123 := LAudioPath + 'lib/Linux/64bit/LibMpg123-64.so';
{$ENDIF}
{$IF DEFINED(cpu86) and defined(linux)}
  LPortAudio := LAudioPath + 'lib/Linux/32bit/LibPortaudio-32.so';
  LMpg123 := LAudioPath + 'lib/Linux/32bit/LibMpg123-32.so';
{$ENDIF}
{$IF DEFINED(cpuarm) and defined(linux)}
  LPortAudio := LAudioPath + 'lib/Linux/arm_raspberrypi/libportaudio-arm.so';
  LMpg123 := LAudioPath + 'lib/Linux/arm_raspberrypi/LibMpg123-arm.so';
{$ENDIF}
{$IFDEF freebsd}
{$IF DEFINED(cpu64)}
  LPortAudio := LAudioPath + 'lib/FreeBSD/64bit/libportaudio-64.so';
  LMpg123 := LAudioPath + 'lib/FreeBSD/64bit/libmpg123-64.so';
{$ELSE}
  LPortAudio := LAudioPath + 'lib/FreeBSD/32bit/libportaudio-32.so';
  LMpg123 := LAudioPath + 'lib/FreeBSD/32bit/libmpg123-32.so';
{$ENDIF}
{$ENDIF}
  
  if AUseSystemLib then
    result := uos_LoadLib('system', nil, 'system', nil, nil, nil)
  else
  begin
    if not FileExists(LPortAudio) then Log(Format('!! File not found [%s]', [LPortAudio]));
    if not FileExists(LMpg123) then Log(Format('!! File not found [%s]', [LMpg123]));
    result := uos_LoadLib(PChar(LPortAudio), nil, PChar(LMpg123), nil, nil,  nil);
  end;
  
  if result > -1 then
  begin 
    for x := 0 to 5 do
    begin
      LSndFile := LSndFilesPath + CFileName[x];
      if not FileExists(LSndFile) then Log(Format('!! File not found [%s]', [LSndFile]));
      LStreams[x] := TMemoryStream.Create;
      LStreams[x].LoadFromFile(PChar(LSndFile)); 
      LStreams[x].Position:= 0;
      uos_CreatePlayer(x);
      uos_AddFromMemoryStream(x, LStreams[x], 1, -1, 0, 1024); 
{$if defined(cpuarm)} // needs lower latency
      uos_AddIntoDevOut(x, -1, 0.8, -1, -1, 0, -1, -1);
{$else}
      uos_AddIntoDevOut(x, -1, 0.3, -1, -1, 0, -1, -1);
{$endif} 
      uos_OutputAddDSPVolume(x, 0, 1, 1); 
    end;
    LLoaded := TRUE;
  end;
end;

procedure SetSoundVolume(AVol: shortint);
var
  x: integer;
begin
  if LLoaded then
    for x := 0 to 5 do
      uos_OutputSetDSPVolume(x, 0, AVol / 100, AVol / 100, TRUE);
end;
   
procedure FreeUos;
begin
  uos_free;
end;
  
end.
