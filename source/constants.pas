
unit Constants;

{
  Document de référence :
  fpGUI/docs/translating_your_application.txt
  
  Voir aussi :
  fpGUI/languages/README.txt
  
  Commandes utiles :
  rstconv -i constants.rsj -o eschecs.en.po
}

interface

resourcestring
  rsEschecs = 'Eschecs';
  rsSave = 'Save game';
  rsQuit = 'Quit';
  rsAbout = 'About Eschecs';
  rsMoves = 'Moves';
  rsComputerMove = 'Computer move';
  rsAutoPlay = 'Autoplay';
  rsBoard = 'Board';
  rsNew = 'New game';
  rsNew960 = 'New chess 960 game';
  rsFlip = 'Flip board';
  rsOptions = 'Options';
  rsColoring = 'Square coloring';
  rsSound = 'Sound';
  rsPromotion = 'Promotion';
  rsKnight = 'Knight';
  rsBishop = 'Bishop';
  rsRook = 'Rook';
  rsQueen = 'Queen';
  rsTitleMessage = 'Message';
  rsStyle = 'Style';
  rsLanguage = 'Language';
  rsVolume = 'Volume';
  rsEnabled = 'Enabled';
  rsAboutMessage = 'Pascal chess program by Roland Chastain';
  rsIllegalMove = 'Illegal move (%s)';
  rsWhiteToMove = 'White to move.';
  rsBlackToMove = 'Black to move.';
  rsWhiteWins = 'White wins.';
  rsBlackWins = 'Black wins.';
  rsCheck = 'Check!';
  rsCheckmate = 'Checkmate!';
  rsStalemate = 'Stalemate!';
  rsDraw = 'Draw!';
  rsWaiting = 'Please wait...';
  rsUciOk = 'UCI protocol accepted. Engine name: %s. Author: %s.';
  rsConnectionFailure = 'Cannot connect to the engine.';
  
implementation

end.
