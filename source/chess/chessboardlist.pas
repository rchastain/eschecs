
{**
@abstract(Liste de positions ou d'échiquiers.)
@longcode(#
  // chesstypes.pas
  PPositionData = ^TPositionData;
  TPositionData = record
    FBoard: TBoard;
    FActive: TPieceColor;
    FCastling: TCastlingRights;
    FEnPassant: string;
    FHalfMoves: integer;
    FFullMove: integer;
  end;
#)
}

unit ChessboardList;

interface

uses
  SysUtils, Classes, ChessTypes, San;
  
type
  TChessboardList = class
    private
      FList: TFPList;
    public
      constructor Create;
      destructor Destroy; override;
      procedure Append(const AData: TPositionData);
      procedure Clear;
      function Count: integer;
      function GetPositionData(const AIndex: integer): TPositionData;
  end;

implementation

const
  CNotFound = -1;
  
constructor TChessboardList.Create;
begin
  inherited Create;
  FList := TFPList.Create;
end;

destructor TChessboardList.Destroy;
begin
  Clear;
  FList.Free;
  inherited Destroy;
end;

procedure TChessboardList.Append(const AData: TPositionData);
var
  LPData: PPositionData;
begin
  New(LPData);
  LPData^ := AData;
  FList.Add(LPData);
end;

procedure TChessboardList.Clear;
var
  i: integer;
begin
  while FList.Count > 0 do
  begin
    i := Pred(FList.Count);
    Dispose(PPositionData(FList.Items[i]));
    FList.Delete(i);
  end;
end;

function TChessboardList.Count: integer;
begin
  result := FList.Count;
end;

function TChessboardList.GetPositionData(const AIndex: integer): TPositionData;
var
  LError: TPositionData;
begin
  if (AIndex >= 0) and (AIndex < FList.Count) then
    result := PPositionData(FList.Items[AIndex])^
  else
  begin
    WriteLn(ErrOutput, '[TChessboardList.GetMove] Index (', AIndex, ') out of bounds');
    FillByte(LError, SizeOf(LError), 0);
    result := LError;
  end;
end;

end.
