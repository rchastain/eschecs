
uses
  SysUtils, Classes, ChessTypes, MoveList, ChessUtils;

function PseudoMove: TMove;
begin
  with result do
  begin
    FX1 := Succ(Random(8));
    FY1 := Succ(Random(8));
    FX2 := Succ(Random(8));
    FY2 := Succ(Random(8));
    FType := ptNil;
    FColor := pcNil;
    FInfo := [];
  end;
end;

var
  LMoveList: TMoveList;
  LMove: TMove;
  i: integer;
  
  LTime: TTime;
  LStrList: TStringList;
  LMoveStr: string;
  
  LSeed: cardinal;
  
const
  N = 9999999;
  
begin
  //Randomize;
  LSeed := GetTickCount64;
  RandSeed := LSeed;
  
  WriteLn('TFPList');
  LTime := Time;  
  LMoveList := TMoveList.Create;  
  for i := 1 to N do
    LMoveList.Append(PseudoMove);
  for i := 0 to Pred(LMoveList.Count) do
  begin
    LMove := LMoveList.GetMove(i);
  end;
  WriteLn(LMoveList.IndexOf('e2e4'));
  LMoveList.Clear;
  LMoveList.Free;
  WriteLn('Time elapsed ', FormatDateTime('nn:ss,zzz', Time - LTime));
  
  RandSeed := LSeed;
  
  WriteLn('TStringList');
  LTime := Time;
  LStrList := TStringList.Create;
  for i := 1 to N do
  begin
    LMove := PseudoMove;
    with LMove do LStrList.Append(MoveToStr(FX1, FY1, FX2, FY2));
  end;
  for i := 0 to Pred(LStrList.Count) do
  begin
    LMoveStr := LStrList[i];
    with LMove do StrToMove(LMoveStr, FX1, FY1, FX2, FY2);
  end;
  WriteLn(LStrList.IndexOf('e2e4'));
  LStrList.Clear;
  LStrList.Free;
  WriteLn('Time elapsed ', FormatDateTime('nn:ss,zzz', Time - LTime));  
end.
