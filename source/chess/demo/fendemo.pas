
{$ASSERTIONS ON}

uses
  SysUtils, Classes, ChessTypes, Fen, FenExamples;

const
  CFile = 'files/fischerandom.fen';
  
var
  LData: TPositionData;
  i: integer;
  
begin
  LData := EncodePositionData(CFenStartPosition);
  Assert(DecodePositionData(LData, FALSE) = CFenStartPosition);
  
  for i := Low(CFenExamples) to High(CFenExamples) do
  begin
    LData := EncodePositionData(CFenExamples[i]);
    WriteLn(CFenExamples[i]);
    WriteLn(
      LData.FCastling[caWH],
      LData.FCastling[caWA],
      LData.FCastling[caBH],
      LData.FCastling[caBA]
    );
    Assert(DecodePositionData(LData, FALSE) = CFenExamples[i]);
  end;
  
  with TStringList.Create do
  try
    LoadFromFile(CFile);
    for i := 0 to Pred(Count) do
    begin
      LData := EncodePositionData(Strings[i]);
      WriteLn(Strings[i]);
      WriteLn(
        LData.FCastling[caWH],
        LData.FCastling[caWA],
        LData.FCastling[caBH],
        LData.FCastling[caBA]
      );
      Assert(DecodePositionData(LData, TRUE) = Strings[i]);
    end;
  finally
    Free;
  end;
end.
