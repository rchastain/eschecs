
{$ASSERTIONS ON}

uses
  SysUtils, Classes, StrUtils, IoUtils, PgnRead;

const
  CSample = 'files/sample.pgn';

var
  LList: TList;
  LGameIdx, LTagIdx, LMoveIdx: integer;
  LGroupIdx: TGroupIndex;
  LGroups: PGroups;
  LFilename: string;
  LTime: TDateTime;
  
begin
  if ParamCount >= 1 then
    LFilename := ParamStr(1)
  else
    LFilename := CSample;
  Assert(FileExists(LFilename));
  
  LTime := Time;
  LList := ParsePgnText(TFile.ReadAllText(LFilename));
  WriteLn('Time elapsed ', FormatDateTime('nn:ss,zzz', Time - LTime));
  
  for LGameIdx := 0 to Pred(LList.Count) do
    with TGameData(LList[LGameIdx]) do
    begin
      WriteLn('LGameIdx=', LGameIdx);
      WriteLn('White=', FTags.Values['White']);
      
      for LTagIdx := 0 to Pred(FTags.Count) do
        WriteLn(
          FTags.Names[LTagIdx], '=',
          FTags.ValueFromIndex[LTagIdx]
        );
      
      for LMoveIdx := 0 to Pred(FMoves.Count) do
      begin
        LGroups := FMoves[LMoveIdx];
        for LGroupIdx := Low(TGroupIndex) to High(TGroupIndex) do
          Write(LGroups^[LGroupIdx], ',');
        WriteLn;
      end;
      
      WriteLn(FTermination);
    end;
  
  for LGameIdx := Pred(LList.Count) downto 0 do
    TGameData(LList[LGameIdx]).Free;
  LList.Free;
end.
