
uses
  ChessGame;

const
  CPos = 'rnknqbbr/ppp1p3/8/6p1/3Pp1p1/2N5/PPPN3P/R1K1QBBR w HAha - 0 8';
  CMove = 'O-O-O';

begin
  with TChessGame.Create(CPos) do
  begin
    WriteLn(CurrPosToStr);
    if IsLegal(CMove) then
    begin
      DoMove(CMove);
      WriteLn(CurrPosToStr);
    end;
    Free;
  end;
end.
