
uses
  ChessGame, Fen;

const
  //CMove = 'e2e4';
  CMove = 'e4';

begin
  with TChessGame.Create(CFenStartPosition) do
  try
    if IsLegal(CMove) then
      DoMove(CMove);
  finally
    Free;
  end;
end.
