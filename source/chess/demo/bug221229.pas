
uses
  ChessGame, Fen;

const
  CMove = 'e1f1';
  
begin
  with TChessGame.Create('brnqkrnb/pppppppp/8/8/8/8/PPPPPPPP/BRNQKRNB w FBfb - 0 1') do
  try
    WriteLn(CurrPosToStr());
    Write(CMove, ' is ');
    if IsLegal(CMove) then
    begin
      WriteLn('legal.');
      DoMove(CMove);
      WriteLn(CurrPosToStr());
    end else
      WriteLn('illegal.');
  finally
    Free;
  end;
end.
